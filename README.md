# Application de gestion des recherches de stages

Cette application permet de gérer les recherches de stages enregistrées. Elle offre des fonctionnalités telles que l'ajout de nouvelles recherches, la modification des informations existantes, et l'affichage des recherches dans une liste.

## Installation

1. Assurez-vous d'avoir Python installé sur votre système.
2. Téléchargez les fichiers du projet.
3. Installez les dépendances nécessaires en exécutant la commande suivante dans votre terminal :

## Utilisation

1. Exécutez le script Python `gestion_recherches_stages.py`.
2. L'interface principale de l'application s'ouvrira, affichant la liste des recherches de stages existantes.
3. Vous pouvez ajouter une nouvelle recherche en cliquant sur le bouton "Ajouter une recherche". Remplissez les informations nécessaires dans la fenêtre qui s'ouvre et cliquez sur "Enregistrer".
4. Pour modifier une recherche existante, cliquez sur le bouton "+" à côté de la recherche correspondante. Cela ouvrira une nouvelle fenêtre où vous pourrez modifier les informations. Cliquez sur "Modifier" pour enregistrer les modifications.
5. Pour supprimer une recherche, vous pouvez le faire directement depuis la base de données en utilisant un outil de gestion de base de données SQLite.

## Structure du code

- `gestion_recherches_stages.py` : Le script principal de l'application qui gère l'interface graphique et les interactions avec la base de données.
- `recherches_stages.db` : La base de données SQLite qui stocke les informations sur les recherches de stages.
